package Lists;
use strict;

use REST::Client;
use JSON qw(decode_json);

my $endpoint;
my $token;
my $timeout = 5; # Seconds.

our $TRUE = 1;
our $FALSE = 0;
our $ERROR = -1;

sub _get_client {
	unless ($endpoint && $token) {
		die 'Please use Lists::init first.';
	}

	my $client = REST::Client->new();
	$client->setTimeout($timeout);

	return $client;
}

sub _get_list_subscription_query {
	my ($subscriber, $list) = @_;

	my $url = $endpoint . $list . '/subscribers/' . $subscriber;
	my $params = 'token=' . $token;

	return $url . '?' . $params;
}

sub _process_subscription_answer {
	my ($client) = @_;

	# Only process sucessful requests.
	if ($client->responseCode() eq '200') {
		my $json = $client->responseContent();
		my $data = decode_json($json);

		return $TRUE if $data->{subscribed} eq 'true';
	} else {
		return $ERROR;
	}

	return $FALSE;
}

sub init {
	($endpoint, $token) = @_;
}

sub is_subscribed {
	my ($subscriber, $list) = @_;
	my $client = _get_client();

	my $query = _get_list_subscription_query($subscriber, $list);
	$client->GET($query);

	return _process_subscription_answer($client);
}

sub subscribe {
	my ($subscriber, $list) = @_;
	my $client = _get_client();

	my $query = _get_list_subscription_query($subscriber, $list);
	$client->POST($query);

	return _process_subscription_answer($client);
}

sub unsubscribe {
	my ($subscriber, $list) = @_;
	my $client = _get_client();

	my $query = _get_list_subscription_query($subscriber, $list);
	$client->DELETE($query);

	return _process_subscription_answer($client);
}
