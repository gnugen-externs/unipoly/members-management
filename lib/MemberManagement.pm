package MemberManagement;
use strict;
use warnings 'FATAL' => 'all';

use Dancer qw/:syntax !pass/;

require Unipoly::Member;
require Unipoly::Group;
require Unipoly::LDAP;
require Lists;

use List::Pairwise qw(grepp);
use Package::Alias
	'MM::PasswordToken' => 'MemberManagement::PasswordToken',
	'MM::RegistrationRequest' => 'MemberManagement::RegistrationRequest';

# Export shared variables for use in submodules.
use Exporter 'import';
our @EXPORT_OK = qw(
	$mailing_lists $public_mailing_lists $unipoly_main_group $unipoly_groups
	$mobile_regex $username_regex
);

our $VERSION = '0.1';

Unipoly::LDAP->url(config->{ldap_url});
Unipoly::LDAP->credentials(
	dn => config->{ldap_admin_dn},
	password => config->{ldap_admin_password});

MM::PasswordToken->init(dbfile => config->{tokens_db});
MM::RegistrationRequest->init(dbfile => config->{registrations_db});

Lists::init(
	config->{mlmmj_web_api_endpoint},
	config->{mlmmj_web_api_token}
);


our $mailing_lists = {
	'membres@unipoly.ch' => {
		description => "The list of Unipoly members",
		locked => 1,
	},
	'achats@unipoly.ch' => {
		description => "Achats Solidaires",
	},
	'api@unipoly.ch' => {
		description => 'Apiculture (Beekeeping)',
	},
	'jardin@unipoly.ch' => {
		description => 'Campus farmers/Jardin (Garden)',
	},
	'sd@unipoly.ch' => {
		description => 'Semaine de la durabilité (Sustainability week)',
	},
	'ie@unipoly.ch' => {
		description => 'Ingénieurs engagés',
	},
	'journal@unipoly.ch' => {
		description => 'Journal d\'Unipoly: Le Canard Huppé',
	},
	'eva@unipoly.ch' => {
		description => 'EVA: Étudiants Véganes et Animalistes',
	},
	'castor@unipoly.ch' => {
		description => 'Castor Freegan: cafétéria autogérée',
	},
	'epicerie@unipoly.ch' => {
		description => 'Épicerie autogérée',
	},
};
our $public_mailing_lists = { grepp { not ($b->{locked}) } %$mailing_lists };

our $unipoly_main_group = 'membres';
our $unipoly_groups = {
	comite => {
			description => 'Comittee members.',
			locked => 1,
	},
	membres => {
			description => 'Active Unipoly members, synced with the members mailing list.',
			locked => 1,
	},
	achats => {
			description => 'Access to the unipoly-achats.epfl.ch webapp.',
			locked => 0,
	},
	journal => {
			description => 'Access to journal.unipoly.ch\'s backend.',
			locked => 0,
	},
	'wordpress-admins' => {
			description => 'Access to unipoly.epfl.ch\'s backend.',
			locked => 1,
	},
	'team-politique' => {
			description => 'Access to the group\'s wiki namespace.',
			locked => 1,
	},
};

our $mobile_regex = '([0-9\s\-]{7,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$';
our $username_regex = '^\w+$';

###
# Load Dancer routes.
load 'MemberManagement/Web/Main.pm';

1;
