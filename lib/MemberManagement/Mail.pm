package MemberManagement::Mail;
use strict;

use Dancer ':syntax';
use Encode;
use Email::Sender::Simple qw(sendmail);
use Email::Simple;
use Email::Simple::Creator;

sub send_registration_confirmation {
	my %args = @_;
	my $recipient = $args{recipient};

	info "Sending registration confirmation for: ?";

	my $msg = template('mail/registration_mail', {
			username => $args{username},
			first_name => $args{first_name},
			first_name => $args{first_name},
			last_name => $args{last_name},
			common_name => $args{common_name},
			email => $args{email},
			mobile_phone => $args{mobile_phone},
			affiliation => $args{affiliation},
			heard_about_us => $args{heard_about_us},
			note => $args{note},
			selected_projects => $args{selected_projects},
		}, {layout => undef});

	my $email = Email::Simple->create(
		header => [
			To => $recipient,
			Subject => 'Unipoly: registration request',
			From    => '"Comité Unipoly" <webmaster@unipoly.ch>',
		],
		body => $msg,
	);

	sendmail($email);
}

sub send_welcome_mail {
	my %args = @_;
	my $username  = $args{member}->username;
	my $recipient = $args{member}->mail;

	info "Sending welcome for $username to $recipient";

	my $msg = template('mail/welcome_mail', {
		password_link => 'https://members.unipoly.ch' . $args{password_link},
		member_link => 'https://members.unipoly.ch/member/' . $username,
	}, {layout => undef});

	my $email = Email::Simple->create(
		header => [
			To => $recipient,
			Subject => 'Welcome to Unipoly',
			From    => '"Comité Unipoly" <webmaster@unipoly.ch>',
		],
		body => $msg,
	);

	sendmail($email);
}

sub send_password_reset {
	my %args = @_;
	my $username  = $args{member}->username;
	my $recipient = $args{member}->mail;

	info "Sending password reset link form $username to $recipient";

	my $msg = template('mail/password_reset_mail', {
		reset_link => 'https://members.unipoly.ch' . $args{password_link},
	}, {layout => undef});

	my $email = Email::Simple->create(
		header => [
			To => $recipient,
			Subject => 'Unipoly account password reset',
			From    => '"Webmaster Unipoly" <webmaster@unipoly.ch>',
		],
		body => $msg,
	);

	sendmail($email);
}

1;
