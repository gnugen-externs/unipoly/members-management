package MemberManagement::RegistrationRequest;
use strict;
use warnings FATAL => 'all';
use 5.10.0;

use DBI;
use Package::Alias
	RegistrationRequest => 'MemberManagement::RegistrationRequest',
	PasswordToken => 'MemberManagement::PasswordToken';

our $RegistrationDBH = undef;

=head1 NAME

RegistrationRequest - Store registrations prior to validation

=cut

=head1 SYNOPSIS

	use RegistrationRequest::init

	# Open connection to database, must be called first.
	RegistrationRequest->init(dbfile => 'registration.db');

	# Create a registration request for user joe
	RegistrationRequest->create('joe');

	# Get a list of current registration requests
	RegistrationRequest->list();

	# Get a specific entry
	RegistrationRequest->get(1);

	# Delete a registration request
	Registration->delete($token)

=cut

sub init {
	my ($class, %args) = @_;
	$RegistrationDBH = DBI->connect("dbi:SQLite:dbname=" . $args{dbfile}, '', '',
		{ RaiseError => 1 }) or die $DBI::errstr;

	$RegistrationDBH->do(<<EOF
create table if not exists Registration (
  id integer primary key autoincrement,
  username varchar not null,
  first_name varchar not null,
  last_name varchar not null,
  common_name varchar not null,
  mail varchar not null,
  mobile varchar not null,
  affiliation varchar not null,
  projects varchar not null,
  how_unipoly varchar not null,
  note varchar not null,
  archived boolean default 0 not null,
  timestamp unsigned integer not null
);
EOF
	) or die $RegistrationDBH->errstr;
}

sub _get_dbh {
	if (defined $RegistrationDBH) {
		return $RegistrationDBH;
	} else {
		die "RegistrationRequest::init must be called first";
	}
}

sub create {
	my ($class, $username, $first_name, $last_name, $common_name, $mobile, $mail,
		$affiliation, $projects, $how_unipoly, $note) = @_;

	for my $arg (($username, $mail)) {
		warn "Missing argument" && return undef unless defined $arg;
	}
	my $dbh = RegistrationRequest::_get_dbh();

	my $sth = $dbh->prepare(
		"INSERT INTO Registration (username, first_name, last_name, common_name,
		mail, mobile, affiliation, projects, how_unipoly, note, timestamp)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);")
		or return undef;

	$sth->execute($username, $first_name, $last_name, $common_name, $mail,
		$mobile, $affiliation, $projects, $how_unipoly, $note, time)
		or return undef;

	return 1;
}

sub list {
	my ($class, $archived) = @_;
	my $dbh = RegistrationRequest::_get_dbh();

	my $sth;
	if (defined $archived) {
		$sth = $dbh->prepare("SELECT * FROM Registration WHERE archived=?;")
			or return undef;
		$sth->execute($archived) or return undef;
	} else {
		$sth = $dbh->prepare("SELECT * FROM Registration;")
			or return undef;
		$sth->execute() or return undef;
	}

	my $entries = $sth->fetchall_arrayref({});

	foreach my $entry (@$entries) {
		$entry->{timestamp_as_string} = POSIX::strftime("%Y-%m-%d", localtime
			$entry->{timestamp});
	}

	return $entries;
}

sub mark_as_reviewed {
	my ($class, $id) = @_;
	warn "Missing argument: id " && return unless defined $id;
	my $dbh = RegistrationRequest::_get_dbh();

	my $sth = $dbh->prepare("UPDATE Registration SET archived=1 WHERE id=?;")
		or return undef;
	$sth->execute($id) or return undef;

	return;
}

sub get {
	my ($class, $id) = @_;
	warn "Missing argument: id " && return unless defined $id;
	my $dbh = RegistrationRequest::_get_dbh();

	my $sth = $dbh->prepare("SELECT * FROM Registration WHERE id=?;")
		or return undef;
	$sth->execute($id) or return undef;
	my $entry = $sth->fetchrow_hashref();

	$entry->{timestamp_as_string} = POSIX::strftime("%Y-%m-%d", localtime
		$entry->{timestamp});

	return $entry;
}

sub delete {
	my ($class, $token) = @_;
	my $dbh = PasswordToken::_get_dbh();

	my $sth = $dbh->prepare("DELETE FROM Token WHERE token = ?;");
	$sth->execute($token);
	return;
}

return 1;
