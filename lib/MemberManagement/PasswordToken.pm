package MemberManagement::PasswordToken;
use strict;
use warnings FATAL => 'all';
use 5.10.0;

use DBI;
use Package::Alias PasswordToken => 'MemberManagement::PasswordToken';

our $TokenDBH = undef;

# Tokens expire after this many seconds
our $EXPIRE = 7 * 24 * 3600;

=head1 NAME

PasswordToken - Keep unique tokens associated with usernames.

=cut

=head1 SYNOPSIS

	use PasswordToken::init

	# Open connection to database, must be called first.
	PasswordToken->init(dbfile => 'token.db');

	# Get a token for user joe
	PasswordToken->create('joe');

	# Get the associated username for a token or undef if invalid or expired
	my $username = PasswordToken->check($token);

	# Delete a token
	PasswordToken->delete($token)

	# Delette all token of a particular user
	PasswordToken->delete_all($user)

	# Get a map of user to the number of valid token they have
	PasswordToken->count_by_user

=cut

sub init {
	my ($class, %args) = @_;
	$TokenDBH = DBI->connect("dbi:SQLite:dbname=" . $args{dbfile}, '', '',
		{ RaiseError => 1 }) or die $DBI::errstr;

	$TokenDBH->do(<<EOF
create table if not exists Token (
  token varchar unique not null,
  user varchar not null,
  timestamp unsigned integer not null
);
EOF
	) or die $TokenDBH->errstr;
}

sub _get_dbh {
	if (defined $TokenDBH) {
		return $TokenDBH;
	} else {
		die "PasswordToken::init must be called first";
	}
}

sub create {
	my ($class, $user) = @_;
	warn "Missing user argument" && return unless defined $user;
	my $dbh = PasswordToken::_get_dbh();

	my $token;
	do {
		$token = _generate_token();
	} while (not _token_unique($token));

	my $sth = $dbh->prepare(
		"INSERT INTO Token (token, user, timestamp) VALUES (?, ?, ?);");
	$sth->execute($token, $user, time);
	return $token;
}

sub check {
	my ($class, $token) = @_;
	warn "Missing token argument" && return unless defined $token;
	my $dbh = PasswordToken::_get_dbh();

	my $sth = $dbh->prepare(
		"SELECT user, timestamp FROM Token WHERE token = ?;");
	$sth->execute($token);
	my ($user, $timestamp) = $sth->fetchrow_array;
	if (_timestamp_expired($timestamp)) {
		PasswordToken::delete($token);
		return undef;
	} else {
		return $user;
	}
}

sub delete {
	my ($class, $token) = @_;
	my $dbh = PasswordToken::_get_dbh();

	my $sth = $dbh->prepare("DELETE FROM Token WHERE token = ?;");
	$sth->execute($token);
	return;
}

sub delete_all {
	my ($class, $user) = @_;
	my $dbh = PasswordToken::_get_dbh();

	my $sth = $dbh->prepare("DELETE FROM Token WHERE user = ?;");
	$sth->execute($user);
	return;
}

sub count_by_user {
	my ($class, $user) = @_;
	my $dbh = PasswordToken::_get_dbh();

	my $sth = $dbh->prepare("SELECT token FROM Token;");
	$sth->execute;
	my %map;
	my $row;
	while ($row = $sth->fetchrow_arrayref) {
		my $token = $row->[0];
		my $user = PasswordToken->check($token);
		if ($user) {
			if (defined($map{$user})) {
				$map{$user} += 1;
			} else {
				$map{$user} = 1;
			}
		}
	}
	return \%map;
}

sub _timestamp_expired {
	my $timestamp = shift;
	if (defined $timestamp) {
		return ($timestamp + $EXPIRE) < time;
	} else {
		return 1;
	}
}

sub _token_unique {
	my $token = shift;
	my $dbh = PasswordToken::_get_dbh();
	my $sth = $dbh->prepare("SELECT COUNT(*) FROM Token WHERE token = ?;");
	$sth->execute($token);
	return not $sth->fetchrow_arrayref->[0];
}

sub _generate_token {
	my $t = "";
	my $count = 48;
	while ($count > 0) {
		my $i = int(rand(64)) + 48;
		if ($i < 58) {
			$t .= chr($i);
		} else {
			$i += 7;
			if ($i < 91) {
				$t .= chr($i);
			} else {
				$i += 6;
				if ($i < 123) {
					$t .= chr($i);
				} elsif ($i == 124) {
					$t .= '-';
				} else {
					$t .= '_';
				}
			}
		}
		$count -= 1;
	}
	return $t;
}
