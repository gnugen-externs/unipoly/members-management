package MemberManagement::Web::Main;
use warnings;
use strict;

use Dancer;
use Dancer::Plugin::FlashMessage;

use Package::Alias
	Mailer => 'MemberManagement::Mail',
	PasswordToken => 'MemberManagement::PasswordToken';

use Exporter 'import';
our @EXPORT_OK = qw(is_admin logged_as password_reset_link check_password_token);

###
# Helper methods.

sub is_admin() {
	my $is_admin = session 'admin';
	return $is_admin if (defined $is_admin);
	my $g1 = Unipoly::Group->new('comite');
	my $g2 = Unipoly::Group->new('webmaster');
	if (($g1 && $g1->contains(session 'username')) || ($g2 && $g2->contains(session 'username'))) {
		session 'admin' => 1;
	} else {
		# Only cache definitive negative lookup: group was found and
		# user is not in it
		session 'admin' => 0;
	}
}

sub logged_as($) {
	my $wanted = shift;
	my $username = session 'username';
	return 0 unless defined $username;
	return $wanted eq session 'username';
}

sub logout {
	session 'username' => undef;
	session 'admin'    => undef;
	session->destroy;
}

sub password_reset_link {
	my $username = shift;
	my $token = PasswordToken->create($username);
	return "/member/" . $username . "/password?token=" . $token;
}

# Return true if the token is valid for the given username.
sub check_password_token {
	my ($username, $token) = @_;
	return unless defined $username;
	my $owner = PasswordToken->check($token);
	return (defined $owner and $username eq $owner);
};

###
# Session control.

hook 'before' => sub {
	unless (request->path_info =~
		m{^/login$|^/register|^/session_expired$|^/member/[^/]+/password|^/passwords$})
	{
		unless (session 'username') {
			info "Redirect to /login from " . request->path_info;
			session 'redirect' => request->path_info;
			return redirect "/login";
		} elsif (not defined session 'timestamp'
				or (session 'timestamp') < time - config->{session_expire}) {
			info "Session of " . session('username') . " has expired.";
			session 'redirect' => request->path_info;
			return redirect "/session_expired";
		} else {
			session 'timestamp' => time;
		}
	}
};

###
# Public and session-related routes.

get '/' => sub {
	if (is_admin) {
		redirect "/admin";
	} else {
		redirect "/member/" . session('username');
	}
};

get '/login' => sub {
	template 'login';
};

any '/session_expired' => sub {
	if (defined session('username')) {
		info session('username') . "logged out due to expired session.";
		logout;
	}

	return redirect '/login';
};

post '/login' => sub {
	my $username = params->{username};
	my $password = params->{password};

	if (defined $username and defined $password) {
		my $member = Unipoly::Member->new(
			username => $username,
			password => $password);
		if (defined $member) {
			info "$username logged in.";
			flash message => "Sucessfully logged in as $username.";
			session 'username' => $username;
			session 'timestamp' => time;
			my $redirect = (session 'redirect') // "/member/$username";
			session 'redirect' => undef;
			$redirect = '/admin' if is_admin;
			return redirect $redirect;
		}
	}

	flash message => "Unable to login.";
	redirect 'login';
};

get '/logout' => sub {
	my $username = session('username');
	info $username . " logs out.";
	logout;
	flash message => "Session for $username sucessfully closed.";
	redirect '/';
};

get '/passwords' => sub {
	return template 'passwords';
};

post '/passwords' => sub {
	if (defined params->{email}) {
		my $member = Unipoly::Member->new(username => params->{email});
		unless ($member) {
			flash message => "No such user";
			return template 'passwords';
		}

		my $token_map = PasswordToken->count_by_user();
		my $max_valid_token_count= 2; # Move this to PasswordToken
		my $current_count = $token_map->{$member->username};
		if ($current_count && $current_count > $max_valid_token_count) {
			flash message => "There already is an active recovery token for this user.";
			return template 'passwords';
		}
		my $link = password_reset_link($member->mail);
		my $mail_sent = eval {
			Mailer::send_password_reset(member => $member, password_link => $link);
		};

		if ($mail_sent) {
			flash message => "Password reset link for " .  $member->username
			. " sent to " . $member->mail;
		} else {
			flash message => "Internal error, could not send mail.";
			warn "Could not send mail: $@";
		}
	}

	redirect '/passwords';
};

###
# Other routes.
load 'MemberManagement/Web/Member.pm';
load 'MemberManagement/Web/Admin.pm';

1;
