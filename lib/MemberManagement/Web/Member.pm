package MemberManagement::Web::Member;
use Dancer::Plugin::EscapeHTML;
use warnings;
use strict;

require Email::Valid;
require Lists;

use Package::Alias Mailer => 'MemberManagement::Mail';

use MemberManagement qw(
	$unipoly_groups $unipoly_main_group $mailing_lists $mobile_regex
	$public_mailing_lists $username_regex
);
use MemberManagement::Web::Main qw(is_admin logged_as check_password_token);

use Dancer;
use Dancer::Plugin::FlashMessage;
use Clone qw(clone);

get '/register' => sub {
	template 'register', {
		'lists' => $public_mailing_lists,
	}
};

post '/register' => sub {
	# Validate required parameters
	my @required_fields = ("username", "first_name", "last_name", "common_name",
		"mail", "mobile", "affiliation", "captcha");
	for my $field (@required_fields) {
		if (!defined params->{$field} || params->{$field} eq '') {
			flash message => "$field is required.";
			return redirect '/register';
		}
	}

	# Validate captcha
	unless (params->{captcha} == length("unipoly")) {
		flash message => "Captcha is invalid.";
		return redirect '/register';
	}

	# Validate mail address
	unless (Email::Valid->address(params->{mail})) {
		flash message => "Mail address is invalid.";
		return redirect '/register';
	}

	# Validate phone number
	unless (params->{mobile} =~ /$mobile_regex/) {
		flash message => "Phone number is invalid.";
		return redirect '/register';
	}

	# Parse selected mailing_lists
	my $selected_mailing_lists = '';
	foreach my $address (keys %$public_mailing_lists) {
		my $value = params->{'list-' . $address};
		if (defined($value) && $value eq 'on') {
			$selected_mailing_lists .= $address . ';';
		}
	}

	my $req = RegistrationRequest->create(
		params->{username},
		params->{first_name},
		params->{last_name},
		params->{common_name},
		params->{mobile},
		params->{mail},
		params->{affiliation},
		$selected_mailing_lists,
		params->{how_unipoly},
		params->{note},
	);

	if (defined $req) {
		flash message => "Registration request for " . escape_html(params->{username}) . " saved.";

		my $mail_sent = eval {
			Mailer::send_registration_confirmation(
				recipient => params->{mail},
				username => params->{username},
				first_name => params->{first_name},
				last_name => params->{last_name},
				common_name => params->{common_name},
				email => params->{mail},
				mobile_phone => params->{mobile},
				affiliation => params->{affiliation},
				heard_about_us => params->{how_unipoly},
				note => params->{note},
				selected_projects => $selected_mailing_lists,
			);
		};

		unless ($mail_sent) {
			flash message => "Could not send confirmation email.";
			warn "Could not send mail: $@";
		}

	} else {
		flash message => "Something went wrong!";
	}

	redirect '/login';
};

get '/member/:username' => sub {
	unless (is_admin or logged_as params->{username}) {
		status '403';
		return "Unauthorized";
	}

	my $member = Unipoly::Member->new(username => params->{username});
	unless (defined $member) {
		status '404';
		return 'Member not found';
	}

	# Resolve group memberships.
	my $group_memberships = clone($unipoly_groups);
	foreach my $group_name (keys %$group_memberships) {
		my $group = Unipoly::Group->new($group_name);
		if (defined $group && $group->contains($member->{username})) {
			$group_memberships->{$group_name}{member} = 1;
		}
	}

	# A member is active if contained in the members group
	my $members = Unipoly::Group->new($unipoly_main_group);
	my $active = $members->contains(params->{username});

	# Resolve mailing list subscriptions.
	my $processed_mailing_lists = clone($mailing_lists);
	foreach my $list (keys %$processed_mailing_lists) {
		my $subscription = Lists::is_subscribed($member->{mail}, $list);
		if ($subscription eq $Lists::TRUE) {
			$processed_mailing_lists->{$list}{subscribed} = 1;
		} elsif ($subscription eq $Lists::ERROR) {
			$processed_mailing_lists->{$list}{error} = 1;
		}
	}

	template 'member', {
		'member' => $member,
		'groups' => $group_memberships,
		'mailing_lists' => $processed_mailing_lists,
		'active' => $active,
	};
};

post '/member/:username' => sub {
	unless (is_admin or logged_as params->{username}) {
		status '403';
		return "Unauthorized";
	}

	my $member = Unipoly::Member->new(username => params->{username});
	unless (defined $member) {
		status '404';
		return 'Member not found';
	}

	my $members = Unipoly::Group->new($unipoly_main_group);
	unless ($members->contains(params->{username})) {
		status '403';
		return 'Member is inactive.';
	}

	my $message;

	# Handle password modification
	if (params->{password} and params->{password_new}
			and params->{password_new_check})
	{
		if (params->{password_new} eq params->{password_new_check}) {
			my $m = Unipoly::Member->new(
				username => params->{username},
				password => params->{password});
			if (defined $m) {
				$m->password(params->{password_new});
				$message .= "Password changed.<br/>";
			} else {
				$message .= "Couldn't change password: old one is wrong.<br/>";
			if (params->{registration_id}) {
				RegistrationRequest->mark_as_reviewed(params->{registration_id});
			}
			if (params->{registration_id}) {
				RegistrationRequest->mark_as_reviewed(params->{registration_id});
			}
			}
		} else {
			$message .= "New passwords don't match.<br/>";
		}
	}

	# Handle uid edit (ONLY if no existing username)
	if (params->{uid} and not $member->uid) {
		if (params->{uid} =~ $username_regex) {
			$member->uid(params->{uid});
			$message .= "Your username has been set.<br/>";
		} else {
			$message .= "The username you specified is malformed.<br/>";
		}
	}

	# Handle mobile modification
	if (params->{mobile} and not params->{mobile} eq $member->mobile) {
		if (params->{mobile} =~ $mobile_regex) {
			$member->mobile(params->{mobile});
			$message .= "Your mobile phone number has been modified.<br/>";
		} else {
			$message .= "The mobile phone you specifed is malformed.<br/>";
		}
	}

# The directory is currently indexed on mail addresses, we cannot allow the
# users to change their address themseve!
# Handle mail modification
#	if (params->{mail}) {
#		my $mail = Email::Valid->address(params->{mail});
#		if ($mail) {
#			if (not $mail eq $member->mail) {
#				$member->mail(params->{mail});
#				$message .= "Your mail address has been modified.<br/>";
#			}
#		} else {
#			$message .= "The mail address you specified is invalid.<br/>";
#		}
#	}

	# Update group memberships.
	foreach my $group_name (keys %$unipoly_groups) {
		unless ($unipoly_groups->{$group_name}{locked}) { # Do not allow updates on locked groups.
			my $group = Unipoly::Group->new($group_name);
			my $value = params->{'group-' . $group_name};
			if (defined $group) {
				if ($group->contains($member->{username})) {
					if (!defined $value) {
						$group->remove($member->{username});
						$message .= 'You have been removed from group ' . $group_name . ".\n";
					}
				} elsif (defined $value && $value eq 'on') {
					$group->add($member->{username});
					$message .= 'You have been added to group ' . $group_name . ".\n";
				}
			} elsif (defined $value && $value eq 'on') {
				$message .= 'Could not add you to unknown group ' .$group_name . ".\n";
			}
		}
	}

	# Handle mailing-list subscription
	foreach my $list (keys %$mailing_lists) {
		unless ($mailing_lists->{$list}{locked}) { # Do not allow updates on locked lists.
			my $value = params->{'list-' . $list};
			if (Lists::is_subscribed($member->{mail}, $list) eq $Lists::TRUE) {
				if (!defined $value) {
					if (Lists::unsubscribe($member->{mail}, $list) eq $Lists::FALSE) {
						$message .= 'You have been removed from: ' . $list;
					} else {
						$message .= 'Could not remove you from: ' . $list;
					}
				}
			} elsif (defined $value && $value eq 'on') {
					if (Lists::subscribe($member->{mail}, $list) eq $Lists::TRUE) {
						$message .= 'You have been added to: ' . $list;
					} else {
						$message .= 'Could not add you to: ' . $list;
					}
			}
		}
	}

	flash message => $message;
	redirect '/member/' . params->{username};
};

get '/member/:username/password' => sub {
	my $username = params->{username};
	my $token = params->{token};
	unless (defined $token) {
		status '403';
		flash message => "Unauthorized (no token)";
		return redirect "/";
	}

	unless (check_password_token($username, $token)) {
		status '403';
		flash message => "Unauthorized (invalid token)";
		return redirect "/";
	}

	template 'password_token', {
		'username' => $username,
		'token' => $token,
	};
};

post '/member/:username/password' => sub {
	my $token = params->{token};
	my $username = params->{username};
	unless (defined $token) {
		status '403';
		return "Unauthorized (no token)";
	}

	unless (check_password_token($username, $token)) {
		status '403';
		return "Unauthorized (no such token)";
	}

	my $message;
	if (params->{password_new} and params->{password_new_check}) {
		if (params->{password_new} eq params->{password_new_check}) {
			my $member = Unipoly::Member->new(username => $username);
			$member->password(params->{password_new});
			PasswordToken->delete_all($username);
			return redirect "/login";
		} else {
			$message .= "Passwords don't match.";
		}
	}

	template 'password_token', {
		'username' => $username,
		'token' => $token,
		'message' => $message,
	};
};

post '/member/:username/disable' => sub {
	unless (is_admin or logged_as params->{username}) {
		status '403';
		return "Unauthorized";
	}

	my $member = Unipoly::Member->new(username => params->{username});
	unless (defined $member) {
		status '404';
		return 'Member not found';
	}

	my $members = Unipoly::Group->new($unipoly_main_group);
	unless ($members->contains(params->{username})) {
		status '403';
		return 'Member is inactive.';
	}

	my $message;

	# Important: remove from member group.
	if ($members->contains($member->{username})) {
		$members->remove($member->{username});
		$message .= 'You have been removed from the member group.';
	}

	# Remove from other known LDAP groups.
	foreach my $group_name (keys %$unipoly_groups) {
		my $group = Unipoly::Group->new($group_name);
		if ($group && $group->contains($member->{username})) {
			$group->remove($member->{username});
			$message .= 'You have been removed from: ' . $group_name . '.';
		}
	}

	# Remove from every mailing list.
	foreach my $list (keys %$mailing_lists) {
		if (Lists::is_subscribed($member->{mail}, $list) eq $Lists::TRUE) {
			if (Lists::unsubscribe($member->{mail}, $list) eq $Lists::FALSE) {
				$message .= 'You have been removed from: ' . $list;
			} else {
				$message .= 'Could not remove you from: ' . $list;
			}
		}
	}

	flash message => $message;
	redirect '/member/' . params->{username};
};

1;
