package MemberManagement::Web::Admin;
use strict;

use MemberManagement qw($public_mailing_lists);
use MemberManagement::Web::Main qw(is_admin password_reset_link);
use Package::Alias
	PasswordToken => 'MemberManagement::PasswordToken',
	RegistrationRequest => 'MemberManagement::RegistrationRequest',
	Mailer => 'MemberManagement::Mail';

use Dancer;
use Dancer::Plugin::FlashMessage;
use Time::Piece;

###
# Helpers.

sub members_sort_func {
	my $ret = $a->{firstName} cmp $b->{firstName};
	if ($ret == 0) {
		$ret = $a->{lastName} cmp $b->{lastName};
	}
	return $ret;
}

sub compute_account_expiration {
	my ($original_time, $semester_count) = @_;

	if ($semester_count < 1) {
		return $original_time;
	}

	my $ts;

	if ($original_time->mon < 6) {
		# If January-May, set expiration to 1st of October
		$ts = Time::Piece->strptime('01-10-' . $original_time->year, '%d-%m-%Y');
	} elsif ($original_time->mon > 11) {
		# If December, set expiration to 1st of October of next year
		$ts = Time::Piece->strptime('01-10-' . ($original_time->year + 1), '%d-%m-%Y');
	} else {
		# If July-November, set expiration to 1st of May of next year
		$ts = Time::Piece->strptime('01-03-' . ($original_time->year + 1), '%d-%m-%Y');
	}
	
	return compute_account_expiration($ts, $semester_count - 1);
}

###
# Routes.

get '/admin' => sub {
	unless (is_admin) {
		status '403';
		return 'Unauthorized';
	}

	my $registration_requests = MemberManagement::RegistrationRequest->list(0);

	template 'admin/home', {
		'registration_requests' => $registration_requests,
	};
};

get '/list/all' => sub {
	unless (is_admin) {
		status '403';
		return 'Unauthorized';
	}

	my $members = Unipoly::Member->all_members;
	my @members = sort members_sort_func values %$members;

	template 'admin/members', {
		'members' => \@members,
		'title' => 'All members (even old ones)',
	}, {
		layout => 'large',
	};
};

get '/subscriptions' => sub {
	unless (is_admin) {
		status '403';
		return "Unauthorized";
	}

	my @members = sort {
		($b->subscription_expire <=> $a->subscription_expire)
			or ($a->username cmp $b->username)
	} values(%{Unipoly::Member->all_members});

	my %active_members_by_year;
	my %inactive_members_by_year;
	foreach my $member (@members) {
		my $year = (localtime $member->subscription_expire)->year;
		if ($member->subscription_active) {
			push @{$active_members_by_year{$year}}, $member;
		} else {
			push @{$inactive_members_by_year{$year}}, $member;
		}
	}

	template 'admin/subscriptions', {
		'members' => \@members,
		'active_members_by_year' => \%active_members_by_year,
		'inactive_members_by_year' => \%inactive_members_by_year,
	}, {
		layout => 'large',
	};
};

post '/subscriptions' => sub {
	unless (is_admin) {
		status '403';
		return "Unauthorized";
	}

	my $members = Unipoly::Member->all_members;
	my $message;

	foreach my $username (keys %$members) {
		my $paid = params->{$username . '-paid'};
		my $date = params->{$username . '-date'};

		my $new_date;
		if (defined $paid && $paid eq 'semester') {
			$new_date = compute_account_expiration(Time::Piece->new, 1);
		} elsif (defined $paid && $paid eq 'year') {
			$new_date = compute_account_expiration(Time::Piece->new, 2);
		} else {
			$new_date = Time::Piece->strptime($date, '%d-%m-%Y');
		}

		if (defined $new_date) {
			my $old_date = localtime $members->{$username}->subscription_expire;
			unless ($old_date == $new_date) {
				$members->{$username}->subscription_expire($new_date->epoch);
				$message .= "Updated subscription expiration for $username.<br/>";
			}
		}
	}

	redirect '/subscriptions';
};

any '/admin/passwords' => sub {
	unless (is_admin) {
		status '403';
		return 'Unauthorized';
	}

	my $message;
	if (defined params->{delete_for}) {
		PasswordToken->delete_all(params->{delete_for});
		$message .= "Tokens for " . params->{delete_for} . " were deleted.";
	}

	my $create_for = params->{create_for};
	if (defined $create_for) {
		my $member = Unipoly::Member->new(username => $create_for);
		if ($member) {
			my $link = password_reset_link($member->mail);
			my $mail_sent = eval {
				Mailer::send_password_reset(member => $member, password_link => $link);
			};

			if ($mail_sent) {
				flash message => "Password reset link for " .  $member->username
				. " sent to " . $member->mail;
			} else {
				flash message => "Internal error, could not send mail.";
				#warn "Could not send mail: $@";
			}
		} else {
			$message .= "No such user";
		}
	}

	template 'admin/passwords', {
		'message' => $message,
		'token_map' => PasswordToken->count_by_user,
	};
};

get '/review/:id' => sub {
	unless (is_admin) {
		status '403';
		return 'Unauthorized';
	}

	my $registration_request = RegistrationRequest->get(params->{id});
	template 'admin/new_member', {
		'mailing_lists' => $public_mailing_lists,
		%$registration_request,
	};
};

post '/archive_registration/:id' => sub {
	unless (is_admin) {
		status '403';
		return 'Unauthorized';
	}

	my $action = RegistrationRequest->mark_as_reviewed(params->{id});
	if (defined $action) {
		return redirect '/admin';
	} else {
		return redirect '/review/' . params->{id};
	}
};


get '/list/registrations' => sub {
	unless (is_admin) {
		status '403';
		return 'Unauthorized';
	}

	my $registration_requests = RegistrationRequest->list();

	template 'admin/registrations', {
		'registration_requests' => $registration_requests,
	};
};

get '/new_member' => sub {
	unless (is_admin) {
		status '403';
		return 'Unauthorized';
	}

	template 'admin/new_member', {
		'mailing_lists' => $public_mailing_lists,
	};
};

post '/new_member' => sub {
	unless (is_admin) {
		status '403';
		return 'Unauthorized';
	}

	my $message;

	# Create the new member
	my @required = qw( common_name first_name last_name mail username );
	if (grep(!defined, map {params->{$_}} @required)) {
		$message = "Some information is missing.";
	} else {
		my $m = Unipoly::Member->create(
			cn => params->{common_name},
			firstName => params->{first_name},
			lastName => params->{last_name},
			mail => params->{mail},
			mobile => params->{mobile},
			username => params->{username},
			password => params->{password},
		);
		if ($m->{entry}) {
			if (params->{paid}) {
				$m->{entry}->subscription(time);
			}
			my $reset_link = password_reset_link($m->{entry}->username);
			my $mail_sent = eval {
				Mailer::send_welcome_mail(
					member => $m->{entry},
					password_link => $reset_link);
			};
			unless ($mail_sent) {
				flash message => "Could not send notification email.";
				warn "Could not send mail: $@";
			}

			# Flag review as processed.
			if (params->{registration_id}) {
				RegistrationRequest->mark_as_reviewed(params->{registration_id});
			}

			# Add user to mailing lists
			foreach my $address (keys %$public_mailing_lists) {
				my $value = params->{'list-' . $address};
				if (defined($value) && $value eq 'on') {
					unless (Lists::subscribe($m->{entry}->{mail}, $address) eq $Lists::TRUE) {
						$message .= "Could not add user to list: $address";
					}
				}
			}

			$message .= 'User was sucessfully created.';
			flash message => $message;
			return redirect '/member/' . $m->{entry}->username;
		} else {
			$message = "Couldn't create new member: " . $m->{err};
		}
	}

	flash message => $message;
	redirect '/review/' . params->{registration_id};
};

any '/admin/passwords' => sub {
	unless (is_admin) {
		status '403';
		return 'Unauthorized';
	}

	my $message;
	if (defined params->{delete_for}) {
		PasswordToken->delete_all(params->{delete_for});
		$message .= "Tokens for " . params->{delete_for} . " were deleted.";
	}

	my $create_for = params->{create_for};
	if (defined $create_for) {
		my $member = Unipoly::Member->new(username => $create_for);
		if ($member) {
			my $link = password_reset_link($member->mail);
			my $mail_sent = eval {
				Mailer::send_password_reset(member => $member, password_link => $link);
			};

			if ($mail_sent) {
				flash message => "Password reset link for " .  $member->username
				. " sent to " . $member->mail;
			} else {
				flash message => "Internal error, could not send mail.";
				#warn "Could not send mail: $@";
			}
		} else {
			$message .= "No such user";
		}
	}

	template 'admin/passwords', {
		'message' => $message,
		'token_map' => PasswordToken->count_by_user,
	};
};

1;
