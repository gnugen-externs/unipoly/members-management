package Unipoly::Member;
use strict;
use warnings;
use 5.10.0;

use Digest::SHA;
use Encode qw(decode);
use MIME::Base64;
use Net::LDAP qw(LDAP_SUCCESS);
use Net::LDAP::Util qw(escape_filter_value escape_dn_value);
use Time::Piece;

use Unipoly::LDAP;

=head1 NAME

Unipoly::Member - Convenient acces to Unipoly member's information stored on
LDAP server.

=cut

=head1 SYNOPSIS

	use Unipoly::Member

	# Set the URL used to connect the LDAP server
	Unipoly::Member->ldap_url('ldaps://ldap.gnugen.ch:636');

	# Set the credentials used for administrative operations
	Unipoly::Member->admin_credentials(
		dn => 'cn=member-management,ou=Services,dc=unipoly,dc=epfl,dc=ch',
		password => 'password');

	# Get someone's instance
	my $member = Unipoly::Member->new(username => 'username');

	# Get someone's instance if his password is valid
	my $member = Unipoly::Member->new(
			username => 'username',
			password => 'password');

	# Get multiple instances at the same time
	my @members = Unipoly::Member->new(usernames => ['foo', 'bar']);

=cut

=head1 METHODS

=head2 Credentials

To set the credentials used to access and modify the LDAP entries call
C<< Unipoly::Member->admin_credentials(dn => $dn, password => $pass) >>
This must be call before using any other functionality of this class otherwise
C<Unipoly::Member> won't be able to access the LDAP server.

=cut

sub admin_credentials {
	my ($class, %args) = @_;
	die "Missing dn parameter" unless defined $args{dn};
	die "Missing password parameter" unless defined $args{password};
	our $Admin_DN = $args{dn};
	our $Admin_Password = $args{password};
}

=head2 new(%params)

Constructor, returns a new instance of this class or undef if the lookup
failed. The available parameters are:

C<username>
Mandatory, the username to look for in the LDAP.

C<password>
Optional, if specified undef will also be returned if the username couldn't
be authentified with the provided password.

Alternatively you can get multiple instances at the same time by passing a
reference to a list of usernames in the C<usernames> parameter. This greatly
speeds up the construction of multiple instances compared to constructing them
in a loop. The ordering of the returned instances is undefined.

=cut

sub new {
	my ($class, %arg) = @_;

	my $ldap = Unipoly::LDAP->ldap;
	return unless (defined $ldap);

	if (defined $arg{usernames}) {
		return $class->_from_usernames($ldap, @{$arg{usernames}});
	}

	# Lookup LDAP for requested people
	my $member;
	if (defined $arg{username}) {
		$member = $class->_from_username($ldap, $arg{username});
	} else {
		die "Missing arg sciper or gaspar";
	}

	# Return undef if we found nobody
	return unless (defined $member->{username});

	# Check password if specified and return undef if it is wrong
	if (defined $arg{password}) {
		return unless ($member->_check_password($ldap, $arg{password}));
	}

	return $member;
}

=pod

C<< Unipoly::Member->all_members >>
Return a hashmap of all members with usernames as keys.

=cut

sub all_members {
	my $class = shift;

	my $ldap = Unipoly::LDAP->ldap;
	return unless (defined $ldap);

	my $res = $ldap->search(
		base => $Unipoly::LDAP::USERS_BASE,
		filter => "(objectClass=unipolyPerson)",
		attrs => ['', '*']);

	my %members;
	my $entry;
	while ($entry = $res->shift_entry) {
		my $m = {};
		bless $m, $class;
		$m->_from_entry($entry);
		$members{$m->{username}} = $m;
	}

	return \%members;
}

=head2 Creating new members

New members can be created with the following code:
	my $m = Unipoly::Member->create {
		cn => 'Foo Bar',
		firstName => 'Foo',
		lastName => 'Bar',
		mail => 'foo@bar.net',
		mobile => '+41791234567',
		username => 'fbar',
	};

=cut

sub create {
	my ($class, %args) = @_;
	for my $arg (qw(cn firstName lastName mail mobile username)) {
		unless (defined $args{$arg}) {
			warn "missing $arg argument";
			return;
		}
	}

	my $ldap = Unipoly::LDAP->ldap;
	return unless (defined $ldap);

	my $userdn = 'mail=' . escape_dn_value($args{mail}) . ','
	. $Unipoly::LDAP::USERS_BASE;
	my @userattrs = (
		cn => $args{cn},
		objectClass => [
			'inetOrgPerson',
			'simpleSecurityObject',
			'organizationalPerson',
			'unipolyPerson'],
		sn => $args{lastName},
		givenName => $args{firstName},
		uid => $args{username},
		mail => $args{mail},
		mobile => $args{mobile},
		userPassword => '',
		unipolyAchatsAdmin => 'FALSE',
		unipolyAchatsEnable => 'FALSE',
		unipolySubscriptionExpireOn => 0,
	);

	# Create the user entry
	my $mesg = $ldap->add($userdn, attrs => \@userattrs);
	if ($mesg->code) {
		return { err => "Failed to add user entry: " . $mesg->error };
	}

	my $member = Unipoly::Member->new(username => $args{mail});
	unless ($member) {
		return  { err => "Couldn't find newly created entry, LDAP is probably in "
			. "an inconsistant state." };
	}

	Unipoly::Group->new("membres")->add($member->username)
		or return { err => "Couldn't add member to members group" };

	return { entry => $member };
}

# Populate the member from its LDAP entry
sub _from_entry {
	my ($self, $entry) = @_;
	return unless defined $entry;
	my $attr;
	$self->{dn} = decode('UTF-8', $entry->dn);
	$self->{name} = decode('UTF-8', $entry->get_value("cn", asref => 1)->[0]);
	# We currently use mail addresses as usernames.
	# $attr = $entry->get_value("uid", asref => 1);
	$attr = $entry->get_value("mail", asref => 1);
	if (defined $attr) {
		$self->{username} = decode('UTF-8', $attr->[0]);
	} else {
		$self->{username} = $self->{dn};
	}
	$attr = $entry->get_value("uid", asref => 1);
	if (defined $attr) {
		$self->{uid} = decode('UTF-8', $attr->[0]);
	}
	$self->{commonName} = decode('UTF-8', $entry->get_value("cn", asref => 1)->[0]);
	$attr = $entry->get_value("givenName", asref => 1);
	if (defined $attr) {
		$self->{firstName} = decode('UTF-8', $attr->[0]);
	} else {
		$self->{firstName} = "";
	}
	$attr = $entry->get_value("sn", asref => 1);
	if (defined $attr) {
		$self->{lastName} = decode('UTF-8', $attr->[0]);
	} else {
		$self->{lastName} = "";
	}
	$attr = $entry->get_value("mail", asref => 1);
	if (defined $attr) {
		$self->{mail} = decode('UTF-8', $attr->[0]);
	} else {
		$self->{mail} = "";
	}
	$attr = $entry->get_value("mobile", asref => 1);
	$self->{mobile} = decode('UTF-8', $attr->[0]) if defined $attr;

	$attr = $entry->get_value("unipolySubscriptionExpireOn", asref => 1);
	if (defined $attr) {
		$self->{"unipolySubscriptionExpireOn"} = decode('UTF-8', $attr->[0]);
	}
}

sub _entry_for_username {
	my ($ldap, $username) = @_;
	die "Missing username argument" unless defined $username;
	my $safe_username = escape_filter_value($username);
	my $mesg = $ldap->search(
		base => $Unipoly::LDAP::USERS_BASE,
		filter => "(&(objectClass=unipolyPerson)(mail=$safe_username))");
	$mesg->code && warn "Can't find $username: " . $mesg->error;
	return $mesg->shift_entry;
}

sub _from_username {
	my ($class, $ldap, $username) = @_;
	my $entry = _entry_for_username($ldap, $username);
	my $member = bless {}, $class;
	$member->_from_entry($entry);
	return $member;
}

sub _from_usernames {
	my ($class, $ldap, @usernames) = @_;
	return unless scalar(@usernames) > 0;

	my $filter = '(&(objectClass=unipolyPerson)(|'
	. join("", map {'(mail=' . escape_filter_value($_) . ')'}
		@usernames)
	. '))';

	my $mesg = $ldap->search(
		base => $Unipoly::LDAP::USERS_BASE,
		filter => $filter);
	$mesg->code && warn "Can't search users: " . $mesg->error;

	# hash instead of array used only due to the way
	# _add_multiple_host_permissions work
	my %members;
	while (my $entry = $mesg->pop_entry) {
		my $member = bless {}, $class;
		$member->_from_entry($entry);
		$members{$member->username} = $member;
	}

	return values %members;
}

sub _check_password {
	my ($self, $ldap, $password) = @_;
	$ldap = Unipoly::LDAP->new;
	my $mesg = $ldap->bind($self->{dn}, password => $password);
	$ldap->disconnect();
	return $mesg->code == LDAP_SUCCESS;
}

sub _modify {
	my $self = shift;
	my %attributes = @_;
	my $ldap = Unipoly::LDAP->ldap;
	return unless (defined $ldap);
	my $entry = _entry_for_username($ldap, $self->{username});
	my $res = $ldap->modify($entry, 'replace' => \%attributes);
	$entry = _entry_for_username($ldap, $self->{username});
	$res->code && warn "Couldn't entry for $self->{username}: " . $res->error;
	$self->_from_entry($entry);
}

=head2 Active

C<< $members->active >>
Returns true if the user is active.

=cut

sub active {
	my ($self) = @_;
	my $members = Unipoly::Group->new('membres');
	return $members->contains($self->{username});
}

=head2 Username

C<< $members->username >>
Returns the member's username.

=cut

sub username {
	my ($self) = @_;
	return $self->{username};
}

=head2 Uid

C<< $members->uid >>
Returns the member's uid.

C<< $member->uid $string >>
Set the uid of the member to the given string. Use with CAUTION.

=cut

sub uid {
	my ($self, $new_value) = @_;
	if (defined $new_value && not defined $self->{uid}) {
		$self->_modify(uid => $new_value);
	}
	return $self->{uid};
}

=head2 Password

C<< $member->password >>
Set a new password for the member. The password will be hashed using SSHA.

=cut

sub _hash_password {
	my $password = shift;
	my $salt;
	for (0..7) { $salt .= chr(int(rand(25)+65)); }
	my $digest = Digest::SHA->new;
	$digest->add($password);
	$digest->add($salt);
	return '{SSHA}' . encode_base64($digest->digest . $salt, '');
}

sub password {
	my ($self, $password) = @_;
	$self->_modify(userPassword => _hash_password($password));
}

=head2 Mobile phone

C<< $member->mobile >>
Return the mobile phone number of the member.

C<< $member->mobile $string >>
Set the mobile phone number of the member to the given string.

=cut

sub mobile {
	my ($self, $new_value) = @_;
	if (defined $new_value) {
		$self->_modify(mobile => $new_value);
	}
	return $self->{mobile};
}

=head2 E-mail address

C<< $member->mail >>
Return the e-mail address of the member.

C<< $member->mail $string >>
Set the e-mail address of the member to the given string.

=cut

sub mail {
	my ($self, $new_value) = @_;
	if (defined $new_value) {
		$self->_modify(mail => $new_value);
	}
	return $self->{mail};
}

=head2 Subscribtion

C<< $member->subscription_active >>
Return true if the member subscription has not expired yet.

C<< $member->subscription_expire >>
Return the UNIX timestamp corresponding to the expiration of the member's
subsciption. Subscriptions expire on 1st March of a given year unless the
subscription was payed after the 1st October of the previous year.

C<< $member->subscription_expire $ts >>
Set the UNIX timestamp corresponding to when the expiration of the member's
subscription. (Number of seconds since 1st January 1970.)

C<< $member->subscription_expire_as_string >>
Return the date when the subscription of the member expires.

=cut

sub subscription_expire {
	my ($self, $ts) = @_;
	if (defined $ts) {
		$self->_modify(unipolySubscriptionExpireOn => $ts);
	}
	if (defined $self->{unipolySubscriptionExpireOn}) {
		return $self->{unipolySubscriptionExpireOn};
	} else {
		return 0;
	}
}

sub subscription_expire_as_string {
	my ($self) = @_;
	my $ts = Time::Piece->strptime($self->subscription_expire, '%s');
	return $ts->strftime('%d-%m-%Y');
}

sub subscription_active {
	my ($self) = @_;
	return time < $self->subscription_expire;
}

1;
