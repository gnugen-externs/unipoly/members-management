# Unipoly Member Management

This [dancer](http://perldancer.org/) webapp allows Unipoly to manage its
directory. It was originally forked from [GNU Generation's member
management](https://gitlab.gnugen.ch/gnugen/members-management), and gained
some features specific to Unipoly such as inscription management.

## CI Pipeline

We make use of gitlab-ci to lint the perl sources (with
[perlcritic](https://metacpan.org/pod/Perl::Critic)) and build the debian
package deployed to our [APT
repository](https://gnugeneration.epfl.ch/membres/it/packages).

## Setup a development environment

You need:
  * [Perl](https://www.perl.org/) >= 5.20.
  * [cpanminus](https://metacpan.org/pod/App::cpanminus`): package manager for perl.
      - `perl-App-cpanminus` on Fedora (`dnf install perl-App-cpanminus`).

You might want to specify where dependencies are installed:

```
export XDG_DATA_HOME=$HOME/.local/share
export PERL5LIB=$XDG_DATA_HOME/perl/lib/perl5
export PERL_CPANM_OPT="--prompt --reinstall -l $XDG_DATA_HOME/perl"
```

Generate the cpanfiles used to install dependencies:

```
cpanm install ExtUtils::MakeMaker Module::CPANfile CPAN::Meta
perl Makefile.PL
./generate-cpanfile.pl
```

Install the application's dependencies:

```
cpanm --instaldeps .
```

Run mlmmj-web:

```
export DANCER_PORT=3000 # Default value, not required
./bin/app.psgi
```

### LDAP

You can use the
[dummy-ldap-container](https://gitlab.gnugen.ch/gnugen-externs/unipoly/dummy-ldap-container)
docker container to run a local LDAP instance similar to the one used by
Unipoly.
